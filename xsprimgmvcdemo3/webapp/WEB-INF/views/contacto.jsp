<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
  <style>
  body, html {
    height: 100%;
    font-family: "Inconsolata", sans-serif;
  }

  .bgimg {
    background-position: center;
    background-size: cover;
    background-image: url("https://retaildesignblog.net/wp-content/uploads/2014/01/Sagamor-lounge-bar-restaurant-by-Andrea-Langhi-Bergamo-Italy.jpg");
    min-height: 75%;
  }

  .menu {
    display: none;
  }
  </style>
  <body>

  <!-- Links (sit on top) -->
  <div class="w3-top">
    <div class="w3-row w3-padding w3-black">
      <div class="w3-col s3">
        <a href="#" class="w3-button w3-block w3-black">CONTACTO</a>
      </div>

    </div>
  </div>

  <!-- Header with image -->
  <header class="bgimg w3-display-container w3-grayscale-min" id="home">
    <div class="w3-display-bottomleft w3-center w3-padding-large w3-hide-small">
      <span class="w3-tag">Nuestro horario: Abrimos cuando llegamos, cerramos cuando nos vamos, y si vienes y no estamos, es que no coincidimos......</span>
    </div>
    <div class="w3-display-middle w3-center">
      <span class="w3-text-white" style="font-size:90px">Tu Bar</span>
    </div>
    <div class="w3-display-bottomright w3-center w3-padding-large">
      <span class="w3-text-white">Es broma...a tu servicio de  6am a 5pm</span>
    </div>
  </header>
  
  
  <div class="w3-container" id="where" style="padding-bottom:32px;">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">Contactanos</span></h5>
    <p>Por que para nosotros eres lo mas importante, te amo Drake</p>
    <p><span class="w3-tag">Si tienes problemas!</span> o alguna duda puedes contactarnos al siguiente correo o llenar el siguiente formulario</p>
    <form action="/action_page.php" target="_blank">
      <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Nombre" required name="Nombre"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="number" placeholder="Correo" required name="Correo"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="problema" placeholder="problema" required name="problema"></p>
      <p><button class="w3-button w3-black" type="submit">ENVIAR MENSAJE</button></p>
    </form>
  </div>
</div>

  <footer class="w3-center w3-light-grey w3-padding-48 w3-large">
    <p>Programado por <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" title="W3.CSS" target="_blank" class="w3-hover-text-green">Rene.Valentin@ids.com.mx</a></p>
  </footer>
  
</html>