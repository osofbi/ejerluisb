<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
  <style>
  body, html {
    height: 100%;
    font-family: "Inconsolata", sans-serif;
  }

  .bgimg {
    background-position: center;
    background-size: cover;
    background-image: url("https://retaildesignblog.net/wp-content/uploads/2014/01/Sagamor-lounge-bar-restaurant-by-Andrea-Langhi-Bergamo-Italy.jpg");
    min-height: 75%;
  }

  .menu {
    display: none;
  }
  </style>
  <body>

  <!-- Links (sit on top) -->
  <div class="w3-top">
    <div class="w3-row w3-padding w3-black">
      <div class="w3-col s3">
        <a href="#" class="w3-button w3-block w3-black">HOME</a>
      </div>
      <div class="w3-col s3">
        <a href="#about" class="w3-button w3-block w3-black">NOSOTROS</a>
      </div>
      <div class="w3-col s3">
        <a href="#menu" class="w3-button w3-block w3-black">MENU</a>
      </div>
      <div class="w3-col s3">
        <a href="#where" class="w3-button w3-block w3-black">DONDE ESTAMOS?</a>
      </div>
    </div>
  </div>

  <!-- Header with image -->
  <header class="bgimg w3-display-container w3-grayscale-min" id="home">
    <div class="w3-display-bottomleft w3-center w3-padding-large w3-hide-small">
      <span class="w3-tag">Nuestro horario: Abrimos cuando llegamos, cerramos cuando nos vamos, y si vienes y no estamos, es que no coincidimos......</span>
    </div>
    <div class="w3-display-middle w3-center">
      <span class="w3-text-white" style="font-size:90px">Tu Bar</span>
    </div>
    <div class="w3-display-bottomright w3-center w3-padding-large">
      <span class="w3-text-white">Es broma...a tu servicio de  6am a 5pm</span>
    </div>
  </header>

  <!-- Add a background color and large text to the whole page -->
  <div class="w3-sand w3-grayscale w3-large">

  <!-- About Container -->
  <div class="w3-container" id="about">
    <div class="w3-content" style="max-width:700px">
      <h5 class="w3-center w3-padding-64"><span class="w3-tag w3-wide">NOSOTROS</span></h5>
      <p>Somos un Café-Bar que te ofrece una opción creada para disfrutar de un ambiente agradable, relajado, sin dejar de lado pasar un rato alegre. </p>
      <p>En cuanto a bebidas se refiere nos enfocamos en atender gustos variados, creamos un estilo atractivo y único para quienes deseen charlar acompañados de una copa de vino, disfrutar de un trago, coctel o cerveza de nuestro bar internacional, y también complacemos los gustos de los amantes del café especialmente preparado.</p>
      <div class="w3-panel w3-leftbar w3-light-grey">
        <p><i>Cocinamos con amor para que comas con conciencia.</i></p>
      </div>
      <img src="https://retaildesignblog.net/wp-content/uploads/2014/01/Sagamor-lounge-bar-restaurant-by-Andrea-Langhi-Bergamo-Italy-10.jpg" style="width:100%;max-width:1000px" class="w3-margin-top">
      <p><strong>Opening hours:</strong> everyday from 6am to 5pm.</p>
      <p><strong>Address:</strong> 15 Adr street, 5015, NY</p>
    </div>
  </div>

  <!-- Menu Container -->
  <div class="w3-container" id="menu">
    <div class="w3-content" style="max-width:700px">

      <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">MENU</span></h5>

      <div class="w3-row w3-center w3-card w3-padding">
        <a href="javascript:void(0)" onclick="openMenu(event, 'Eat');" id="myLink">
          <div class="w3-col s6 tablink">Eat</div>
        </a>
        <a href="javascript:void(0)" onclick="openMenu(event, 'Drinks');">
          <div class="w3-col s6 tablink">Drink</div>
        </a>
      </div>

      <div id="Eat" class="w3-container menu w3-padding-48 w3-card">
        <h5>Bread Basket</h5>
        <p class="w3-text-grey">Assortment of fresh baked fruit breads and muffins 5.50</p><br>

        <h5>Honey Almond Granola with Fruits</h5>
        <p class="w3-text-grey">Natural cereal of honey toasted oats, raisins, almonds and dates 7.00</p><br>

        <h5>Belgian Waffle</h5>
        <p class="w3-text-grey">Vanilla flavored batter with malted flour 7.50</p><br>

        <h5>Scrambled eggs</h5>
        <p class="w3-text-grey">Scrambled eggs, roasted red pepper and garlic, with green onions 7.50</p><br>

        <h5>Blueberry Pancakes</h5>
        <p class="w3-text-grey">With syrup, butter and lots of berries 8.50</p>
      </div>

      <div id="Drinks" class="w3-container menu w3-padding-48 w3-card">
        <h5>Coffee</h5>
        <p class="w3-text-grey">Regular coffee 2.50</p><br>

        <h5>Chocolato</h5>
        <p class="w3-text-grey">Chocolate espresso with milk 4.50</p><br>

        <h5>Corretto</h5>
        <p class="w3-text-grey">Whiskey and coffee 5.00</p><br>

        <h5>Iced tea</h5>
        <p class="w3-text-grey">Hot tea, except not hot 3.00</p><br>

        <h5>Soda</h5>
        <p class="w3-text-grey">Coke, Sprite, Fanta, etc. 2.50</p>
      </div>  
      <img src="https://retaildesignblog.net/wp-content/uploads/2014/01/Sagamor-lounge-bar-restaurant-by-Andrea-Langhi-Bergamo-Italy-03.jpg" style="width:100%;max-width:1000px;margin-top:32px;">
    </div>
  </div>

  <!-- Contact/Area Container -->
  <div class="w3-container" id="where" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
      <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">DONDE ESTAMOS?</span></h5>
      <p>En tu kokoro chiquita ;)</p>
      <img src="https://retaildesignblog.net/wp-content/uploads/2014/01/Sagamor-lounge-bar-restaurant-by-Andrea-Langhi-Bergamo-Italy-04.jpg" class="w3-image" style="width:100%">
      <p><span class="w3-tag">Visitanos!!!!!!</span> Nuestra música es selecta y variada, escogida por nuestros DJ´s, la cual se acompaña de videos musicales que se transmiten en varias TV´s plasmas distribuidas estratégicamente para visión desde todos nuestros ambientes, que a la vez son un punto clave para la transmisión de deportes que sólo pueden disfrutarse en servicio pagado,  también encuentras servicio de Wi-Fi, amplio parqueo propio y seguro.</p>

    </div>
  </div>

  <!-- End page content -->
  </div>

  <!-- Footer -->
  <footer class="w3-center w3-light-grey w3-padding-48 w3-large">
    <p>Programado por <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" title="W3.CSS" target="_blank" class="w3-hover-text-green">Rene.Valentin@ids.com.mx</a></p>
  </footer>

  <script>
  // Tabbed Menu
  function openMenu(evt, menuName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("menu");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
    }
    document.getElementById(menuName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-dark-grey";
  }
  document.getElementById("myLink").click();
  </script>

  </body>
</html>