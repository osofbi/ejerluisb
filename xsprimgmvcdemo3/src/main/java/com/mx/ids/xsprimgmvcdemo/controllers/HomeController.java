package com.mx.ids.xsprimgmvcdemo.controllers;

import java.util.Optional;

//import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

//import com.mx.ids.xsprimgmvcdemo.model.User;
import com.mx.ids.xsprimgmvcdemo.service.ServiceABC;


@Controller
public class HomeController {
	
	@Autowired
	ServiceABC service;
	
	@Value ("${var.x.value}")
	private String valPath =" Empty ";
	
	
	/*@RequestMapping(value="/home",method=RequestMethod.GET)
	public String doHome(Locale locale,Model model) {
		
		System.out.println("Home Page Request, locale = "+ locale);
		model.addAttribute("serverTime", service.metodoNegocio(locale));
		System.out.println(" el valor es >>>>>"+ valPath);
		
		System.out.println("Hooomeee");
		return "home";
	}
	
	@RequestMapping(value="/contacto",method=RequestMethod.GET)
	public String contacto(Locale locale,Model model) {
		System.out.println("contacto");
		return "contacto";
	}
	
	@RequestMapping(value="/que",method=RequestMethod.GET)
	public String Reneinc(Locale locale,Model model) {
		
		System.out.println("reneinc");
		return "reneinc";
	}
	
	
	@RequestMapping (value = "/user", method = RequestMethod.POST)
	public String user(@Validated User user, Model model) {
		System.out.println("User Page Requested");
		model.addAttribute("userName", user.getUserName());
		return "user";
		
	}*/

	
	
	@RequestMapping(value="/tem",method=RequestMethod.GET)
	public String Tem(@RequestParam(name="nombre") Optional<String> name ,Model model)//,required=false,defaultValue="NO_TIENE" 
	{
		
		System.out.println("Template01");
		model.addAttribute("message", "Hola mundo!!! >>> "+name);
		return "Template01";
	}
	
	/*@GetMapping("/tem/{name}")
	public String Tem(@PathVariable(name="nombre") Optional<String> name ,Model model)//,required=false,defaultValue="NO_TIENE" 
	{
		
		System.out.println("Template01");
		model.addAttribute("message", "Hola mundo!!! >>> "+name);
		return "Template01";
	}*/
	
}
