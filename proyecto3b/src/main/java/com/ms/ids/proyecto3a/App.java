package com.ms.ids.proyecto3a;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mx.ids.test.service.Mensaje;
import com.mx.ids.test.service.Servicio;


public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");

    	
       	Mensaje mensaje=appContext.getBean("mailservice_sing",Mensaje.class);
       	mensaje.enviarmensaje("Renedefenitivo@gmail.com");
    	
       	System.out.println("********************************************************");
       	
       	mensaje=appContext.getBean("mailservice_proto",Mensaje.class);
       	mensaje.enviarmensaje("otro@hotmail.com");
       	
       	System.out.println("********************************************************");
       	Servicio beanService=appContext.getBean("servicio",Servicio.class);
    	System.out.println(beanService.greet());
       	
    	((ClassPathXmlApplicationContext) appContext).close();
 
    }
}
