package com.mx.ids.helloworld;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mx.ids.helloworld.service.GreetingService;

public class App 
{
    public static void main( String[] args )
    {
        //System.out.println( "Hello World!" );
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	GreetingService beanService=null;
    	beanService=(GreetingService)appContext.getBean("greetingService");
    	beanService=appContext.getBean("greetingService",GreetingService.class);
    	beanService=appContext.getBean(GreetingService.class);
    	System.out.println(beanService.greet());
    	((ClassPathXmlApplicationContext) appContext).close();
    }
}
