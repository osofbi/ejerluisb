package com.mx.ids.helloworld.service;

public class GreetingServiceImp implements GreetingService {

	private String msg;
	
	public GreetingServiceImp() {
	}
	
	public GreetingServiceImp(String msg) {
		this.msg = msg;
	}

	@Override
	public String greet() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
