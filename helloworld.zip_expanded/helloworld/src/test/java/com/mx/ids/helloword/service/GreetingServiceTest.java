package com.mx.ids.helloword.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.mx.ids.helloworld.service.GreetingService;
import com.mx.ids.helloworld.service.GreetingServiceImp;



public class GreetingServiceTest {
	
	private GreetingService service;
	
	@Test
	public void testGreeting() {
		
		String param ="Hola mundo ";
		
		service = new GreetingServiceImp(param);
		String message = service.greet();
		
		assertEquals(param, message);
	}

}
