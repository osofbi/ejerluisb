package com.mx.ids.test.service;

public class ServicioI implements Servicio {
	private String msg;

	@Override
	public String greet() {
		return msg;
	}

	public ServicioI(String msg) {
		this.msg = msg;
	}

	public ServicioI() {
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	

}
