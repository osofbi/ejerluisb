package com.ms.ids.proyecto3a;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mx.ids.test.service.Mensaje;
import com.mx.ids.test.service.Servicio;


public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	
       	Mensaje mensaje=appContext.getBean("mailservice",Mensaje.class);
       	mensaje.enviarmensaje("Renedefenitivo@gmail.com");
       	
    	Servicio beanService=null;
    	beanService=(Servicio)appContext.getBean("servicio_by_name");
    	System.out.println(beanService.greet());
       	
       	
       	
       	
    	((ClassPathXmlApplicationContext) appContext).close();
 
    }
}
