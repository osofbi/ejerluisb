package mx.com.ids.proxy;


public class Proxy implements ICuenta {

	
	private ICuenta cuentaCliente;

	public Proxy(ICuenta cuentaReal) {
		this.cuentaCliente = cuentaReal;
	}
	
	@Override
	public Cuenta retirar(Cuenta cuenta, double monto) {
		if (cuentaCliente == null) {
			cuentaCliente = new CuentaBancoAImpl();
			return cuentaCliente.retirar(cuenta, monto);
		} else {
			return cuentaCliente.retirar(cuenta, monto);
		}		
	}

	@Override
	public Cuenta depositar(Cuenta cuenta, double monto) {
		if (cuentaCliente == null) {
			cuentaCliente = new CuentaBancoAImpl();
			return cuentaCliente.depositar(cuenta, monto);
		} else {
			return cuentaCliente.depositar(cuenta, monto);
		}		
	}

	@Override
	public void mostrarS(Cuenta cuenta) {
		if (cuentaCliente == null) {
			cuentaCliente = new CuentaBancoAImpl();
			cuentaCliente.mostrarS(cuenta);
		} else {
			cuentaCliente.mostrarS(cuenta);
		}
	}

}
