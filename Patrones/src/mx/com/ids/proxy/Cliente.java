package mx.com.ids.proxy;

public class Cliente {
	public static void main(String[] args) {
		Cuenta c = new Cuenta(1, "Rene", 10);
		
		ICuenta cuentaProxy = new Proxy(new CuentaBancoAImpl());
		cuentaProxy.mostrarS(c);
		c = cuentaProxy.depositar(c, 50);
		c = cuentaProxy.retirar(c, 20);
		cuentaProxy.mostrarS(c);
		
	}
}
