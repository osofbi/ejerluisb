package mx.com.ids.proxy;

public interface ICuenta {
	Cuenta retirar(Cuenta cuenta, double monto);
	Cuenta depositar(Cuenta cuenta, double monto);
	void mostrarS(Cuenta cuenta);
}
