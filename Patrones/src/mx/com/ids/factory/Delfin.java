package mx.com.ids.factory;

public class Delfin implements Mamifero {
	private String andar;

	public Delfin() {
		super();
		this.andar = "Yo nado";
	}

	public Delfin(String andar) {
		super();
		this.andar = andar;
	}

	@Override
	public String andar() {
		return andar;
	}

}
