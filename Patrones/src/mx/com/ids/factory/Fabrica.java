package mx.com.ids.factory;

public class Fabrica implements Fabrica_abstracta {
	public final static String can="canguro";
	public final static String del="delfin";
	@Override
	public Mamifero CreaMamifero(String animal, String anda) {
		switch(animal) {
		case can:return new Canguro(anda);
		case del:return new Delfin(anda);
		default:return null;
		}
		
	}

}
