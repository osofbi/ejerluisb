package mx.com.ids.factory;

public class Cliente {

	public static void main(String[] args) {
		Fabrica f= new Fabrica();
		Mamifero canguro = f.CreaMamifero("canguro", "corre!!!!!");
		Mamifero delfin = f.CreaMamifero("delfin", "nada!!!!!");
		System.out.println("El canguro "+canguro.andar());
		System.out.println("El delfin "+delfin.andar());

	}

}
