package mx.com.ids.factory;

public class Canguro implements Mamifero {
	private String andar;

	public Canguro() {
		super();
		this.andar = "Yo salto";
	}

	public Canguro(String andar) {
		super();
		this.andar = andar;
	}

	@Override
	public String andar() {

		return andar;
	}

}
