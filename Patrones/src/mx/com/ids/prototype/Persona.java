package mx.com.ids.prototype;

public class Persona implements IPersona {
	private String nombre;
	private int edad;
	
	public Persona(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}

	public Persona() {
		super();
		this.nombre = "No asignado";
		this.edad = 0;
	}


	@Override
	public IPersona clonar() {
		Persona p = null;

		try {
			p = (Persona) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return p;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + "]";
	}
	

}
