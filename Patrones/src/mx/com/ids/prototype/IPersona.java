package mx.com.ids.prototype;

public interface IPersona extends Cloneable{
	IPersona clonar();
}
