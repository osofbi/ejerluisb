package mx.com.ids.prototype;

public class Cliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Persona p1 = new Persona();
		p1.setNombre("Rene");
		p1.setEdad(25);
		Persona p2 = new Persona();
		
		Persona pClonada = (Persona) p1.clonar();

		System.out.println(p1);
		System.out.println(p2);
		System.out.println(pClonada);
		System.out.println(p1 == pClonada);
	}

}
