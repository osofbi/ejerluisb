package com.mx.ids.webdemo01.controllers;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mx.ids.webdemo01.model.User;
import com.mx.ids.webdemo01.service.ServiceABC;

@Controller
public class HomeController {
	
	@Autowired
	ServiceABC service;
	
	@Value ("${var.x.value}")
	private String valPath =" Empty ";
	
	@RequestMapping (value = "/home", method = RequestMethod.GET)
	public String home (Locale locale,Model model) {
		System.out.println("Home Page Request, locale = "+ locale);
		model.addAttribute("serverTime", service.metodoNegocio(locale));
		
		System.out.println(" el valor es >>>>>"+ valPath);
		return "home";
	}
	
	@RequestMapping (value = "/user", method = RequestMethod.POST)
	public String user(@Validated User user, Model model) {
		System.out.println("User Page Requested");
		model.addAttribute("userName", user.getUserName());
		return "user";
		
	}

}