package com.mx.ids.xsprimgmvcdemo.service;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Service;

@Service
public class ServiceABC {
	
	public String metodoNegocio(Locale locale) {
		Date date = new Date ();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG,locale);
		return dateFormat.format(date);
	}

}