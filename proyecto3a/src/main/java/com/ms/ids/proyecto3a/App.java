package com.ms.ids.proyecto3a;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mx.ids.test.service.Mensaje;
import com.mx.ids.test.service.Servicio;


public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
    	Servicio beanService=null;
    	//beanService=(Servicio)appContext.getBean("servicio_constructor");
    	//beanService=appContext.getBean(Servicio.class);
    	beanService=appContext.getBean("servicio_constructor",Servicio.class);
    	System.out.println(beanService.greet());
    	
    	beanService=appContext.getBean("servicio_constructor2",Servicio.class);
    	System.out.println(beanService.greet());
    	
    	beanService=appContext.getBean("servicio_metodo",Servicio.class);
    	System.out.println(beanService.greet());
    	
    	beanService=appContext.getBean("servicio_metodo2",Servicio.class);
    	System.out.println(beanService.greet());
    	
       	Mensaje mensaje=appContext.getBean("mailservice",Mensaje.class);
       	mensaje.enviarmensaje("Renedefenitivo@gmail.com");
    	
    	((ClassPathXmlApplicationContext) appContext).close();
 
    }
}
