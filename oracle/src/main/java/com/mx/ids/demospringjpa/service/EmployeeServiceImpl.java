/**
 * 
 */
package com.mx.ids.demospringjpa.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.ids.demospringjpa.entities.Employee;
import com.mx.ids.demospringjpa.repository.EmployeeRepository;


@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository repository;
	
	
	public void run() {
		List<Employee> employees = createEmployees();

		System.out.println(" -- saving employees --");
        System.out.println(employees);
        repository.saveAll(employees);
		
		
		
		System.out.println(repository.findAll() );
		
		
	}
	
	private List<Employee> createEmployees() {
        return Arrays.asList(
                Employee.create("Rene", "Admin"),
                Employee.create("Mike", "Sale"),
                Employee.create("Rose", "IT")
        );

    }

}