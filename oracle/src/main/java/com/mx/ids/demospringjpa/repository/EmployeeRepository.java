package com.mx.ids.demospringjpa.repository;

import org.springframework.data.repository.CrudRepository;

import com.mx.ids.demospringjpa.entities.Employee;

public interface EmployeeRepository 
	extends CrudRepository<Employee, Long> {

}
