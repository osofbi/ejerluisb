package com.mx.ids.demospringjpa;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mx.ids.demospringjpa.service.EmployeeService;


public class EmployeeMain {


	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(AppConfig.class);
		
		EmployeeService service = context.getBean(EmployeeService.class);
		service.run();
		
		
		EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);
		emf.close();
		
		context.close();
	}

}
