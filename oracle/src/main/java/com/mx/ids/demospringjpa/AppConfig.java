package com.mx.ids.demospringjpa;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@ComponentScan
@EnableJpaRepositories


public class AppConfig {
	@Bean
	EntityManagerFactory entityManagerFactory() {
		
		EntityManagerFactory emf= Persistence
				.createEntityManagerFactory("example-unit");
		return emf;
		
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager txmanager= new JpaTransactionManager();
		txmanager.setEntityManagerFactory(entityManagerFactory());
		return txmanager;
	}
}


