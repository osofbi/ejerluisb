/**
 * 
 */
package com.mx.ids.demospringjpa.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Employee {
	
	@Id @GeneratedValue
	private Long id;
	private String name;
	private String dept;
	
	public static Employee create(String name, String dept) {
		Employee e = new Employee();
		e.setName(name);
		e.setDept(dept);
		return e;
	}
	
	public Employee() {}
	

	public Employee(String name, String dept) {
		super();
		this.name = name;
		this.dept = dept;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", dept=" + dept + "]";
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}
	
}
